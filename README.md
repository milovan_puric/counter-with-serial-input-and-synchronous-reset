# Counter with Serial Input and Synchronous Reset

Full Custom Design of Counter with serial input and synchronous reset.

- Software: Cadence Virtuoso
- Design: Full Custom, Manhattan style
- Technologies: 180nm
- Supply voltage: 1.8V
- Characteristics: fmax = 0.85GHz, surface area = 2136.14um2


---

