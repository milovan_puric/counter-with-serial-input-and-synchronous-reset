simulator lang=spectre
Vdd (vdd! 0) vsource dc=1.8
Gnd (gnd! 0) vsource dc=0
v0 (CLK 0) vsource type=pulse val0=0 val1=1.8 delay=5n rise=0.005n fall=0.005n width=9.995n period=20n
